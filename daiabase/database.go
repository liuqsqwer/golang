package database

import (
	"crypto/md5"
	"fmt"
	"os"
	"time"

	"github.com/howeyc/gopass"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func init() {
	dsn := "root:123456@tcp(172.14.1.27:3306)/classmates?charset=utf8mb4&parseTime=True&loc=Local"
	var err error
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("连接失败：", err)
		return

	}
	fmt.Println(DB)
	fmt.Println("连接成功")

}

var DB *gorm.DB

type Classmates struct {
	ID         int
	Name       string
	Sn         string
	Tel        string
	Comments   string
	Classid    int
	Createtime time.Time
	Classroom  string
	Pswd       string
	Role       string
	Birthday   time.Time
	Email      string
}

var Loginuser Classmates

func Login() bool {
	var name string
	fmt.Println("请输入登录名字")
	fmt.Scanln(&name)

	var pswd string
	fmt.Println("请输入密码")
	pswdbytes, err := gopass.GetPasswdPrompt("密码", true, os.Stdin, os.Stdout)
	if err != nil {
		fmt.Println("密码输入错误", err.Error())
		return false
	}
	pswd = string(pswdbytes)

	DB.Raw(`select * from classmates.classmates 
	where name=?`, name).Scan(&Loginuser)
	if Loginuser.Role == "root" {
		fmt.Println("管理员登录使用")
		return false
	}
	pswdbyts := fmt.Sprintf("%x", md5.Sum([]byte(pswd)))
	if Loginuser.Pswd == pswdbyts {
		fmt.Println("登录成功,你好", Loginuser.Name)
		return true
	} else {
		fmt.Println("你的密码不对")
		return false
	}
}
